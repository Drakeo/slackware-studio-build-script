#!/bin/sh

# Copyright 2012  Patrick J. Volkerding, Sebeka, Minnesota, USA
# All rights reserved.
#
# Copyright 2013 Chess Griffin <chess.griffin@gmail.com> Raleigh, NC
# Copyright 2013 Willy Sudiarto Raharjo <willysr@slackware-id.org>
# All rights reserved.
#
# Based on the xfce-build-all.sh script by Patrick J. Volkerding
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# Set to 1 if you'd like to install/upgrade package as they are built.
# This is recommended.
# multi thread compression for txz
export XZ_DEFAULTS="-T 0"
#We need jdk development look for openjdk or jdk this is needed to build portmidi
 if [ -f  /etc/profile.d/openjdk.sh ]; then
     JAVA="openjdk.sh"   
 fi
 
 if [ -f  /etc/profile.d/jdk.sh ]; then
     JAVA="jdk.sh"
 fi
     
 if [ -f  /etc/profile.d/$JAVA ]; then
       . /etc/profile.d/$JAVA
 else
  echo -e "\e[1;33m you must install JDK or openjdk.\e[0m"   
  echo -e "\e[1;33m http://www.slackware.com/~alien/slackbuilds/openjdk .\e[0m" 
   exit 1
fi

# Building bluray plugin requires Apache Ant:
if ! which ant 1>/dev/null 2>/dev/null ; then
  echo -e "\e[1;33m If  Apache Ant is installed open terminal  with kdesu  or login as root \e[0m"  
  echo -e "\e[1;33m ANT not present - the build will abort on compiling bluray support! .\e[0m"   
  echo -e "\e[1;33m Press Ctrl-C within 10 seconds, install 'apache-ant' and login again .\e[0m" 
  echo -e "\e[1;33m http://www.slackware.com/~alien/slackbuilds/apache-ant.\e[0m" 
  sleep 10
  exit
 fi


INST=1

# This is where all the compilation and final results will be placed
TMP=${TMP:-/tmp/studio/pkgs}
BLOG=${BLOG:-/tmp/studio/buildlog}
mkdir -p $BLOG
# This is the original directory where you started this script
STUDIOROOT=$(pwd)

# Loop for all packages
for dir in \
  buildstudio/chromaprint \
  buildstudio/zita-convolver \
  buildstudio/zita-alsa-pcmi \
  buildstudio/zita-resampler \
  buildstudio/jack2 \
  buildstudio/fltk \
  buildstudio/libmp4v2 \
  buildstudio/xvidcore \
  buildstudio/mxml \
  buildstudio/libass \
  buildstudio/libfdk-aac \
  buildstudio/celt \
  buildstudio/set_rlimits \
  buildstudio/libebur128 \
  buildstudio/hidapi \
  buildstudio/QUSB \
  buildstudio/soundtouch \
  buildstudio/faad2 \
  buildstudio/serd \
  buildstudio/sord \
  buildstudio/lv2 \
  buildstudio/suil \
  buildstudio/sratom \
  buildstudio/lilv \
  buildstudio/python-gflags \
  buildstudio/google-apputils \
  buildstudio/python-dateutil\
  buildstudio/protobuf \
  buildstudio/ladspa_sdk  \
  buildstudio/libshout \
  buildstudio/portaudio \
  buildstudio/portmidi \
  buildstudio/libopenmpt \
  buildstudio/libopenmpt-modplug \
  buildstudio/vamp-plugin-sdk \
  buildstudio/rubberband \
  buildstudio/pulseaudio \
  buildstudio/alsa-plugins \
  buildstudio/ffmpeg \
  buildstudio/vlc \
  buildstudio/pipewire \
  buildstudio/audacity \
  buildstudio/mixxx \
  buildstudio/aubio \
  buildstudio/liblo \
  buildstudio/raptor \
  buildstudio/liblrdf \
  buildstudio/ardour \
  buildstudio/hydrogen \
  buildstudio/qtractor \
  buildstudio/rakarrack \
  buildstudio/yoshimi \
  buildstudio/guitarix \
  buildstudio/ssr \
  buildstudio/qjackctl \
  ; do
  # Get the package name
  package=$(echo $dir | cut -f2- -d /) 
  
  # Change to package directory
  cd $STUDIOROOT/$dir || exit 1 

  # Get the version
  version=$(cat ${package}.SlackBuild | grep "VERSION:" | cut -d "-" -f2 | rev | cut -c 2- | rev)
  build=$(cat ${package}.SlackBuild | grep "BUILD:" | cut -d "-" -f2 | rev | cut -c 2- | rev)
  tag=$(cat ${package}.SlackBuild | grep "TAG:" | cut -d "-" -f2 | rev | cut -c 2- | rev)
  #get arch 
  ARCH=$(uname -m)
  # Check for duplicate sources
  sourcefile="$(ls -l $STUDIOROOT/$dir/${package}-*.tar.?z* | wc -l)"
  if [ $sourcefile -gt 1 ]; then
    echo "You have following duplicate sources:"
    ls $STUDIOROOT/$dir/${package}-*.tar.?z* | cut -d " " -f1
    echo "Please delete sources other than ${package}-$version to avoid problems"
    exit 1
  fi
  
  # The real build starts here
  sh ${package}.SlackBuild |tee -a $BLOG/${package}.log || exit 1
  if [ "$INST" = "1" ]; then
    PACKTXT="${package}-$version"
    PACKAGE="${package}-$version-*.t?z"
    PACKINF=$(ls $TMP | grep $PACKTXT)
    if [ -f $TMP/$PACKAGE ]; then
      upgradepkg --install-new --reinstall $TMP/$PACKAGE
      cat $STUDIOROOT/$dir/slack-desc |tail -n 11 > $TMP/$PACKTXT-$ARCH-$build$tag.txt
      md5sum $TMP/$PACKAGE > $TMP/$PACKINF.md5
#      gpg --armor --detach-sign --passphrase  $TMP/$PACKAGE
    else
      echo "Error:  package to upgrade "$PACKAGE" not found in $TMP"
      exit 1
    fi
  fi
  
  # back to original directory
  cd $STUDIOROOT
done
