#!/bin/bash
removepkg ardour
removepkg aubio
removepkg audacity
removepkg celt 
removepkg chromaprint
removepkg faad2
removepkg google-apputils
removepkg guitarix
removepkg hidapi
removepkg hydrogen
removepkg jack2
removepkg ladspa_sdk
removepkg libass
removepkg libebur128 
removepkg libfdk-aac
removepkg liblo
removepkg liblrdf
removepkg libmp4v2
removepkg libopenmpt 
removepkg libopenmpt-modplug
removepkg libshout
removepkg lilv
removepkg live555
removepkg lv2
removepkg mixxx
removepkg mxml 
removepkg portaudio 
removepkg portmidi  
removepkg protobuf
removepkg python-dateutil
removepkg python-gflags
removepkg qjackctl
removepkg qtractor
removepkg QUSB
removepkg rakarrack
removepkg raptor
removepkg rubberband
removepkg serd
removepkg set_rlimits
removepkg sord  
removepkg soundtouch
removepkg sratom
removepkg srt    
removepkg suil 
removepkg vamp-plugin-sdk
removepkg vlc
removepkg x264 
removepkg x265
removepkg xvidcore
removepkg yoshimi  
removepkg zita-convolver
removepkg zita-alsa-pcmi
removepkg zita-resampler
# reinstall the  default slackware packages.
slackpkg reinstall alsa-plugins 
slackpkg reinstall ffmpeg
slackpkg reinstall fltk
slackpkg reinstall pipewire
slackpkg reinstall pulseaudio 
slackpkg reinstall ssr 
slackpkg upgrade alsa-plugins 
slackpkg upgrade ffmpeg
slackpkg upgrade fltk
slackpkg upgrade pipewire
slackpkg upgrade pulseaudio 
slackpkg upgrade ssr
